﻿using Common;
using ShopOnline.BLL;
using ShopOnline.Models.OtherEF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ShopOnline.Controllers
{
    public class BaseController<T> : CustomController where T : class, new()
    {
        #region Variable
        protected UnitOfWork Instance;
        #endregion

        #region Contructor
        public BaseController(UnitOfWork unitOfWork)
        {
            Instance = unitOfWork;
        }
        #endregion

        #region Virtual
        public virtual ActionResult Index()
        {
            return View(Instance.GetRepository<T>().GetItems(1));
        }

        public virtual ActionResult Pages(int? pageIndex)
        {
            if (!pageIndex.HasValue || pageIndex == 0 || pageIndex == 1)
                return RedirectToAction("Index");

            return View("Index", Instance.GetRepository<T>().GetItems(pageIndex.Value));
        }

        public virtual ActionResult Detail(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);

            T item = Instance.GetRepository<T>().FindItem(id.Value);
            if (item == null)
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);

            return View(item);
        }

        public virtual ActionResult Create()
        {
            return View(new T());
        }

        public virtual ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);

            T item = Instance.GetRepository<T>().FindItem(id.Value);
            if (item == null)
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);

            return View(item);
        }

        public virtual ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);

            if (Instance.GetRepository<T>().FindItem(id.Value) == null)
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);

            return View();
            //return new HttpStatusCodeResult(System.Net.HttpStatusCode.Found);
        }

        public virtual ActionResult InitItem()
        {
            return Ok(new T());
        }

        public virtual ActionResult GetItem(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            T item = Instance.GetRepository<T>().FindItem(id.Value);
            if (item == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            return Ok(item);
        }

        [HttpPost]
        public virtual ActionResult CreateItem(DataRequest<T> data)
        {
            try
            {
                Instance.BeginTransaction();
                Instance.GetRepository<T>().AddOrUpdate(data.NewData);
                Instance.SaveChanges();
                Instance.CommitTransaction();
                return Ok(data.NewData);
            }
            catch (Exception ex)
            {
                Instance.RollbackTransaction();
                return BadRequest(ex);
            }
        }

        [HttpPost]
        public virtual ActionResult CreateItems(DataRequest<T>[] datas)
        {
            try
            {
                Instance.BeginTransaction();
                Instance.GetRepository<T>().AddOrUpdate(datas.Select(x => x.NewData).ToArray());
                Instance.SaveChanges();
                Instance.CommitTransaction();
                return Ok(datas.Select(x => x.NewData).ToArray());
            }
            catch (Exception ex)
            {
                Instance.RollbackTransaction();
                return BadRequest(ex);
            }
        }

        [HttpPut]
        public virtual ActionResult EditItem(DataRequest<T> data)
        {
            try
            {
                Instance.BeginTransaction();
                Instance.GetRepository<T>().AddOrUpdate(data.NewData);
                Instance.SaveChanges();
                Instance.CommitTransaction();
                return Ok(data.NewData);
            }
            catch (Exception ex)
            {
                Instance.RollbackTransaction();
                return BadRequest(ex);
            }
        }

        [HttpPut]
        public virtual ActionResult EditItems(DataRequest<T>[] datas)
        {
            try
            {
                Instance.BeginTransaction();
                Instance.GetRepository<T>().AddOrUpdate(datas.Select(x => x.NewData).ToArray());
                Instance.SaveChanges();
                Instance.CommitTransaction();
                return Ok(datas.Select(x => x.NewData).ToArray());
            }
            catch (Exception ex)
            {
                Instance.RollbackTransaction();
                return BadRequest(ex);
            }
        }

        [HttpDelete]
        public virtual ActionResult DeleteItem(T item)
        {
            return View();
        }

        [HttpDelete]
        public virtual ActionResult DeleteItems(T[] items)
        {
            return View();
        }
        #endregion
    }
}