﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ShopOnline.Controllers
{
    public interface IJsonResult
    {
        JsonResult Ok();
        JsonResult BadRequest();
        JsonResult NoContent();
        JsonResult NotFound();

        JsonResult Ok(string message);
        JsonResult BadRequest(string message);
        JsonResult NoContent(string message);
        JsonResult NotFound(string message);

        JsonResult Ok(object obj);
        JsonResult BadRequest(object obj);
        JsonResult NoContent(object obj);
        JsonResult NotFound(object obj);

        JsonResult BadRequest(Exception exception);

        JsonResult Ok(string key, object value);
        JsonResult BadRequest(string key, object value);
        JsonResult NoContent(string key, object value);
        JsonResult NotFound(string key, object value);

        JsonResult Ok(Dictionary<string, object> keyValues);
        JsonResult BadRequest(Dictionary<string, object> keyValues);
        JsonResult NoContent(Dictionary<string, object> keyValues);
        JsonResult NotFound(Dictionary<string, object> keyValues);
    }
    public class CustomController : Controller, IJsonResult
    {
        public JsonResult Ok()
        {
            return new CustomJsonResult(HttpStatusCode.OK);
        }
        public JsonResult BadRequest()
        {
            return new CustomJsonResult(HttpStatusCode.BadRequest);
        }
        public JsonResult NoContent()
        {
            return new CustomJsonResult(HttpStatusCode.NoContent);
        }
        public JsonResult NotFound()
        {
            return new CustomJsonResult(HttpStatusCode.NotFound);
        }

        public JsonResult Ok(string message)
        {
            return new CustomJsonResult(HttpStatusCode.OK, message);
        }
        public JsonResult BadRequest(string message)
        {
            return new CustomJsonResult(HttpStatusCode.BadRequest, message);
        }
        public JsonResult NoContent(string message)
        {
            return new CustomJsonResult(HttpStatusCode.NoContent, message);
        }
        public JsonResult NotFound(string message)
        {
            return new CustomJsonResult(HttpStatusCode.NotFound, message);
        }

        public JsonResult Ok(object obj)
        {
            return new CustomJsonResult(HttpStatusCode.OK, obj);
        }
        public JsonResult BadRequest(object obj)
        {
            return new CustomJsonResult(HttpStatusCode.BadRequest, obj);
        }
        public JsonResult NoContent(object obj)
        {
            return new CustomJsonResult(HttpStatusCode.NoContent, obj);
        }
        public JsonResult NotFound(object obj)
        {
            return new CustomJsonResult(HttpStatusCode.NotFound, obj);
        }

        public JsonResult BadRequest(Exception exception)
        {
            //ModelStateDictionary modelState = new ModelStateDictionary();
            //modelState.AddModelError("Exception", exception);
            //return new CustomJsonResult(HttpStatusCode.BadRequest, modelState);
            return new CustomJsonResult(HttpStatusCode.BadRequest, exception.Message);
        }

        public JsonResult Ok(string key, object value)
        {
            return new CustomJsonResult(HttpStatusCode.OK, key, value);
        }
        public JsonResult BadRequest(string key, object value)
        {
            return new CustomJsonResult(HttpStatusCode.BadRequest, key, value);
        }
        public JsonResult NoContent(string key, object value)
        {
            return new CustomJsonResult(HttpStatusCode.NoContent, key, value);
        }
        public JsonResult NotFound(string key, object value)
        {
            return new CustomJsonResult(HttpStatusCode.NotFound, key, value);
        }

        public JsonResult Ok(Dictionary<string, object> keyValues)
        {
            return new CustomJsonResult(HttpStatusCode.OK, keyValues);
        }
        public JsonResult BadRequest(Dictionary<string, object> keyValues)
        {
            return new CustomJsonResult(HttpStatusCode.BadRequest, keyValues);
        }
        public JsonResult NoContent(Dictionary<string, object> keyValues)
        {
            return new CustomJsonResult(HttpStatusCode.NoContent, keyValues);
        }
        public JsonResult NotFound(Dictionary<string, object> keyValues)
        {
            return new CustomJsonResult(HttpStatusCode.NotFound, keyValues);
        }
    }
    public class CustomJsonResult : JsonResult
    {
        private int _statusCode;

        public CustomJsonResult(HttpStatusCode statusCode)
        {
            _statusCode = Convert.ToInt32(statusCode);
        }
        public CustomJsonResult(HttpStatusCode statusCode, string message)
        {
            _statusCode = Convert.ToInt32(statusCode);
            Data = message;

        }
        public CustomJsonResult(HttpStatusCode statusCode, object data)
        {
            _statusCode = Convert.ToInt32(statusCode);
            Data = data;
        }
        public CustomJsonResult(HttpStatusCode statusCode, string key, object value)
        {
            _statusCode = Convert.ToInt32(statusCode);
            //Data = new KeyValuePair<string, object>(key, value);
            Data = new { key, value };
        }
        public CustomJsonResult(HttpStatusCode statusCode, Dictionary<string,object> keyValues)
        {
            _statusCode = Convert.ToInt32(statusCode);
            //Data = new KeyValuePair<string, object>(key, value);
            Data = keyValues;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            MaxJsonLength = int.MaxValue;
            ContentType = "application/json";
            JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            context.RequestContext.HttpContext.Response.StatusCode = _statusCode;
            base.ExecuteResult(context);
        }
    }
}