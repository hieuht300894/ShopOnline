﻿var controls = {
    txtUsername: $('#username'),
    txtPassword: $('#password'),
    lbUsername: $('#username_msg'),
    lbPassword: $('#password_msg'),
    chkIsRemember: $('#isRemember'),
    btnLogin: $('#btnLogin'),
    frmGoogleSignIn: $('#frmGoogleSignIn'),
    frmGoogleSignOut: $('#frmGoogleSignOut')
};

//$(controls.txtUsername).off('change').on('change', function () {
//    defaultFunction.ElementOff(controls.lbUsername);
//    defaultFunction.ResetText(controls.lbUsername);
//});

//$(controls.txtPassword).off('change').on('change', function () {
//    defaultFunction.ElementOff(controls.lbPassword);
//    defaultFunction.ResetText(controls.lbPassword);
//});

$(controls.btnLogin).off('click').on('click', function () {
    defaultFunction.ElementOff(controls.lbUsername);
    defaultFunction.ResetText(controls.lbUsername);
    defaultFunction.ElementOff(controls.lbPassword);
    defaultFunction.ResetText(controls.lbPassword);

    $.ajax({
        url: window.location.origin + '/Login/NormalAuthorization',
        contentType: defaultValue.Json_ContentType,
        method: defaultValue.Post,
        data: JSON.stringify({
            Username: $(controls.txtUsername).val(),
            Password: $(controls.txtPassword).val(),
            IsRemember: $(controls.chkIsRemember).is(':checked')
        }),
        success: function (result) {
            if (result.Username != undefined) {
                defaultFunction.ElementOn(controls.lbUsername);
                defaultFunction.SetText(controls.lbUsername, result.Username);
            }
            if (result.Password != undefined) {
                defaultFunction.ElementOn(controls.lbPassword);
                defaultFunction.SetText(controls.lbPassword, result.Password);
            }
            if (result.NotFound != undefined) {
                defaultFunction.ElementOn(controls.lbUsername);
                defaultFunction.SetText(controls.lbUsername, result.NotFound);
            }
            if (result.Disable != undefined) {
                defaultFunction.ElementOn(controls.lbUsername);
                defaultFunction.SetText(controls.lbUsername, result.Disable);
            }
            if (result.Success != undefined) {
                window.location.href = result.Success;
            }
            console.log(result);
        },
        error: function (result) {
            console.log(result);
        }
    });
});

$(controls.frmGoogleSignIn).off('click').on('click', function () {
    $(this).submit();
});

$(controls.frmGoogleSignOut).off('click').on('click', function () {
    $(this).submit();
});
