using ShopOnline.Models.General;
using ShopOnline.Models.Interface;

namespace ShopOnline.Models.EF
{
    public class xPermissionDetail : Master, IPermissionGroup, IPermission
    {
        public int IDPermissionGroup { get ; set ; }
        public string PermissionGroupCode { get ; set ; }
        public string PermissionGroupName { get ; set ; }
        public int IDPermission { get ; set ; }
        public string Controller { get ; set ; }
        public string Action { get ; set ; }
        public string Method { get ; set ; }
        public string Template { get ; set ; }
        public string Path { get ; set ; }
    }
}
