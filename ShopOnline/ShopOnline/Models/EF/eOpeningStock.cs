﻿using ShopOnline.Models.General;
using ShopOnline.Models.Interface;
using System;

namespace ShopOnline.Models.EF
{
    public class eOpeningStock : Master, IProductGroup, IProduct, IUnit, IWarehouse
    {
        public int IDProduct { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public int IDUnit { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public int IDWarehouse { get; set; }
        public string WarehouseCode { get; set; }
        public string WarehouseName { get; set; }
        public System.DateTime Date { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public decimal TotalQuantity { get; set; }
        public int IDProductGroup { get ; set ; }
        public string ProductGroupCode { get ; set ; }
        public string ProductGroupName { get ; set ; }
    }
}
