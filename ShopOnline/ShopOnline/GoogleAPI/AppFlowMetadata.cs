﻿using Common;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Drive.v2;
using Google.Apis.Plus.v1;
using Google.Apis.Util.Store;
using System;
using System.Reflection;
using System.Web.Mvc;

namespace ShopOnline.GoogleAPI
{
    public class AppFlowMetadata : FlowMetadata
    {
        static readonly string[] scopes = new string[] {
            PlusService.Scope.PlusLogin,
            PlusService.Scope.UserinfoEmail,
            PlusService.Scope.UserinfoProfile,
            DriveService.Scope.Drive
        };
        static readonly IAuthorizationCodeFlow flow = new AuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
        {
            ClientSecrets = new ClientSecrets
            {
                ClientId = Define.Instance.GoogleClientID,
                ClientSecret = Define.Instance.GoogleClientSecret
            },
            Scopes = scopes,
            DataStore = new FileDataStore(GoogleWebAuthorizationBroker.Folder),
           
        });

        public override IAuthorizationCodeFlow Flow
        {
            get
            {
                return flow;
            }
        }

        public override string GetUserId(Controller controller)
        {
            // In this sample we use the session to store the user identifiers.
            // That's not the best practice, because you should have a logic to identify
            // a user. You might want to use "OpenID Connect".
            // You can read more about the protocol in the following link:
            // https://developers.google.com/accounts/docs/OAuth2Login.

            try
            {
                var user = controller.Session["user"];
                if (user == null)
                {
                    user = Guid.NewGuid();
                    controller.Session["user"] = user;
                }
                return user.ToString();
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return string.Empty;
            }

        }
    }
}
