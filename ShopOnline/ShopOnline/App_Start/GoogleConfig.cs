﻿using Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ShopOnline
{
    public class GoogleConfig
    {
        public static void GetClientID()
        {
            Define.Instance.GoogleClientID = ConfigurationManager.AppSettings["GoogleClientID"] ?? string.Empty;
            Define.Instance.GoogleClientSecret = ConfigurationManager.AppSettings["GoogleClientSecret"]?? string.Empty;
        }
    }
}